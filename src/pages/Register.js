

import { Fragment,useEffect ,useState} from 'react';
import { Navigate , useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

import Swal from 'sweetalert2';



export default function Register() {

  const [email , setEmail] = useState('');
  const [password , setPassword] = useState('');
  const [user , setUser] = useState('');

  const navigate = useNavigate();

  const [isActive , setIsActive] = useState(false)

  useEffect(()=>{
    if(email !== "" && password !== ""){
        setIsActive(true)
    }else{
        setIsActive(false)
    }

},[email,password])

function register(event){
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
        method: "POST",
        body: JSON.stringify({
         
          email: email,
          password: password,
        }),
        headers:{
            'Content-Type' : 'application/json'
        }
      })
      .then(result => result.json())
      .then(data => {
        
        if(data){
          Swal.fire({
            title: "Successfully Registered",
            icon: "success",
            text: "Please login you account"
          })
          navigate("/login")
        } else {
          Swal.fire({
            title: "Registration unsuccessful",
            icon: "error",
            text: "Please try again"
          })
        }
      })
    }

  return (
   
   <> 
           <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="w-full max-w-md space-y-8">
          <div>
           
            <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
             Register
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Or{' '}
              <Link to = "/login"><a href="#" className="font-medium text-orange-500 hover:text-orange-900">
                If you already have account Login
              </a></Link>
            </p>
          </div>
          <form className="mt-8 space-y-6" onSubmit={event => register(event)}>
            <input type="hidden" name="remember" defaultValue="true" />
            <div className="-space-y-px rounded-md shadow-sm text-center">
              <div>
                <label htmlFor="email-address" className="not-sr-only">
                  Email address
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  value = {email}
                  onChange ={event => setEmail(event.target.value)} 
                  required
                  className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  placeholder="Email address"
                />
              </div>
              <div >
                <label htmlFor="password" className="not-sr-only">
                Password
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  value = {password}
                  onChange ={event => setPassword(event.target.value)} 
                  required
                  className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  placeholder="Password"
                />
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <input
                  id="remember-me"
                  name="remember-me"
                  type="checkbox"
                  className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                />
                <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              <div className="text-sm">
                <a href="#" className="font-medium text-orange-500 hover:text-orange-900">
                  Forgot your password?
                </a>
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="group relative flex w-full justify-center rounded-md border border-transparent bg-orange-400 py-2 px-4 text-sm font-medium text-white hover:bg-orange-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              >
               
                Sign in
              </button>
            </div>
          </form>
        </div>
      </div>
      
    
    </>
  )

 
}

