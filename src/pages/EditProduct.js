
import EditModal from "../Components/EditModal"

export const setEditModalShow = (props) =>{

    const[product ,setProduct] = useState([])


    useEffect(()=>{
        //fetch all products
        fetch(`${process.env.REACT_APP_API_URL}/product`)
        .then(result => result.json())
        .then(data => {
           
            
            setProduct(data.map (product => {

                    return (
                        <EditModal key = {product._id}editProductProp = {product}/>
                    )
                }))
        })

    },[])
}