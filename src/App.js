import AppNavbar from './Components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import { Navigate } from 'react-router-dom';
import {BrowserRouter as Router, Routes , Route} from 'react-router-dom'
import { UserProvider } from './UserContext';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Notfound from './pages/404page';
import Product from './pages/Product';
import CreateProduct from './pages/CreateProduct';
import AdminItems from './pages/AdminItems';
import AllUsers from './pages/AllUser';
import MyOrder from './pages/MyOrder';




import { useState , useEffect } from 'react';
import Items from './Components/Items';
import Checkout from './Components/Checkout';


function App() {

    const [user , setUser] = useState(null);

    const [product, setProduct] = useState([])



      const [cart, setCart] = useState([]);
    
   function addItemToCart(e) {
        const item = e.target.value;
       
        setCart([...cart, item]);

      }

 
  
  
    

    useEffect(()=>{
    
    },[user])
    
    const unSetUser = () =>{
      localStorage.clear()
}

useEffect(()=>{

  fetch(`${process.env.REACT_APP_API_URL}/user/details`,{
    headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
    }
 })
 .then(result => 
    result.json()
 ).then(data => {
    
    if(localStorage.getItem('token')!== null){
      setUser({
        id:data._id,
        isAdmin: data.isAdmin
    })
    }else{
      setUser(null)
    }
  })




},[])
  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
      <AppNavbar/>
     

      <Routes>
      <Route path="/" element = {<Home/>}/>
      <Route path="/items" element = {<Product/>}/>
      <Route path="/addProduct" element = {<CreateProduct/>}/>
      <Route path="/AdminItems" element = {<AdminItems/>}/>
      <Route path="/myOrder" element = {<MyOrder/>}/>
      <Route path="/product/:productId" element = {<Checkout/>}/>
     
      

      

      <Route path="/AllUsers" element = {<AllUsers/>}/>
      <Route path="/login" element = {<Login/>}/>
      <Route path="/register" element = {<Register/>}/>
      <Route path="/logout" element = {<Logout/>}/>
      <Route path="/Not-found" element = {<Notfound/>}/> 
     <Route path="*" element = {<Navigate to = "Not-found"/>}/>


      
    </Routes>
    
    </Router>
    </UserProvider>
  )
    
}

export default App;
