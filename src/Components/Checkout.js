
import { useContext ,useState, useEffect , Fragment} from 'react';
import { Button, Modal ,Space} from 'antd';
import Swal from "sweetalert2";
import { useNavigate ,useParams } from "react-router-dom";



export default function Checkout(){


    const navigate = useNavigate()


    const [postProductName ,setPostProductName] = useState("");

    const [postDescription, setPostDescription] = useState("");

    const [postPrice, setPostPrice] = useState("");

    const[quantity, setPostQuantity] =useState("")

    const {productId} = useParams();


    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
        .then(result => result.json())
        .then(data => {

             setPostProductName(data.postProductName);
             setPostDescription(data.postDescription);
             setPostPrice(data.postPrice)
        })
    },[productId])

    
    
    const checkOutProduct = (id) => {
       
        fetch(`${process.env.REACT_APP_API_URL}/product/createOrder/${id}`, {
           method: "POST",
           body:JSON.stringify({


            quantity: quantity

           }),
           headers:{
            'Content-Type': 'application/json',
               Authorization: `Bearer ${localStorage.getItem('token')}`
           }
         })
         .then(result => result.json())
         .then(data => {
        
           if(data){
               Swal.fire({
                   title: "Success",
                   icon: "success",
                   text: "Thank you for purchasing"
                 })
              
             navigate("/items")
           } else {
               Modal.error({
                   title: 'Update failed',
                   content: 'Please Try Again',
                 });
           }
         })
       }


    return(
        <>
        <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed  w-full h-full bg-black opacity-40"
                           
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-lg p-4 mx-auto bg-white rounded-md shadow-lg">
                                <div className="mt-3 sm:flex">

                          <div className="mx-auto bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
           
                <img className="p-8 rounded-t-lg" src="https://i.ibb.co/RD9szv9/Meowtech-Ultra-Premium-Cat-Litter-12-18-L-Lavender-Scent-Ultra-Fine-Sand-with-Activated-Charcoal.webp" alt="product image" />
          
            <div className="px-5 pb-5">
                <a href="#">
                    <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white max-w-prose">{postProductName}</h5>
                </a>
                
                <h6>{postDescription}</h6>
            </div>
            <div className="flex items-center justify-between px-5 pb-5 ">
                    <span className="text-3xl font-bold text-gray-900 dark:text-white max-w-prose" >{postPrice}</span>
                    
               <button
                   type="button" 
                   onClick={() => checkOutProduct(productId)}
                   className="max-w-prose text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">CheckOut
                   </button>
                </div>
            
            </div>
           
                         </div>
                                  </div>
                             </div>
                         </div>
    
</>
    )

}