import { useContext ,useState, useEffect } from 'react';
import { Button, Modal ,Space} from 'antd';
import UserContext from '../UserContext';
import Swal from "sweetalert2";
import { useNavigate ,useParams } from "react-router-dom";
import {Link , NavLink} from 'react-router-dom';





export default function Items({productProp, handleAddtoCart}){

    const { _id ,productName, description ,price ,productImage} = productProp;

    const{user}= useContext(UserContext)

    const [showModal, setShowModal] = useState(false);

    



    return(
        <>
            <section class="py-10">
                <div class="mx-auto grid">
                    <article class="rounded-xl bg-white p-3 shadow-lg hover:shadow-xl hover:transform hover:scale-105 duration-300 ">
                         <div class="relative  items-end overflow-hidden rounded-xl">
                             <img src="https://i.ibb.co/RD9szv9/Meowtech-Ultra-Premium-Cat-Litter-12-18-L-Lavender-Scent-Ultra-Fine-Sand-with-Activated-Charcoal.webp"/>
                             
                        <div class="mt-1 p-2">
                            <h2 class="text-slate-700">{productName}</h2>
                            <p class="mt-1 text-sm text-slate-400">{description}</p>

                            <div class="mt-3 flex items-end justify-between">
                                <p class="text-lg font-bold text-blue-500">PHP {price}</p>

                                <div class="flex items-center space-x-1.5 rounded-lg bg-blue-500 px-4 py-1.5 text-white duration-100 hover:bg-blue-600">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-4 w-4">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                                </svg>

                                {
                                    user?
                                    <Link to = {`/product/${_id}`}><button class="text-sm">Buy now!</button></Link>
                                    
                                    :

                                    <Link to = {`/login`}><button class="text-sm">Login first</button></Link>
                                }

                                
                                </div>
                            </div>
                        </div>
                         </div>
                    </article>
                 </div>

            </section>
        



             

            
           
           </>   
          
    )
}