

import React from "react";

export default function ViewOrders({OrderProp}){

    const { _id ,userId, products} = OrderProp;

    return(

        <tbody className="bg-white flex-1 sm:flex-none">
                <tr className="hover:bg-gray-100 flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                     <td className="p-4 text-clip overflow-hidden whitespace-normal text-sm font-normal text-gray-500">
                            <div className="text-base font-semibold text-gray-900">{_id}</div>
                               
                    </td>
                    <td className="p-4 text-clip overflow-hidden whitespace-normal text-base font-medium text-gray-900">{userId}</td>
                    
                    <td className="p-4 text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{products}</td>
                    <td className="p-4 text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{products}</td>
                  
                </tr> 
            </tbody>
    )
}