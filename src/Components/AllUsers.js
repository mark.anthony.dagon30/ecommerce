import { Fragment } from "react"



export default function AllUsers({UserProp}){

    const{_id,firstName,lastName,email,phoneNumber,isAdmin}= UserProp


    return(

        <>
            <Fragment>
          
            <tbody className="bg-white block md:table-row-group">
                <tr className="hover:bg-gray-100  border border-grey-500 md:border-none block md:table-row">
                     <td className="p-4 text-clip overflow-hidden whitespace-normal text-sm font-normal text-gray-500">
                        <div className="text-base font-semibold text-gray-900">{_id}</div>
                               
                    </td>
                    <td className="p-4 block md:table-cell text-clip overflow-hidden whitespace-normal text-base font-medium text-gray-900">{firstName}</td>
                    
                    <td className="p-4 block md:table-cell text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{lastName}</td>
                    <td className="p-4 block md:table-cell text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{email}</td>
                    <td className="p-4 block md:table-celltext-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{phoneNumber}</td>
           
                    <td>
                    <div className="sticky sm:flex items-center w-full sm:justify-between bottom-0 right-0  border-gray-200 p-4 hover:bg-gray-100">
                        <div className="flex items-center mb-4 sm:mb-0">
                            <button type="button" className="flex-1 text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium inline-flex items-center justify-center rounded-lg text-sm px-3 py-2 text-center mr-4">Update</button>

                        </div>
                       
                    </div>
                    </td>  
                </tr> 
            </tbody>
            </Fragment>
        </>
    )
}