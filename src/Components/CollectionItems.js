
import React, { Fragment, useState ,useEffect } from "react"
import { Button, Modal ,Space} from 'antd';
import Swal from "sweetalert2";
import { useNavigate ,useParams } from "react-router-dom";






export default function CollectionItems({adminProductProp}){

    const { _id ,productName, description ,price,category, productImage ,isActive} =adminProductProp;

    const [updateProductname, setUpdateProductName] = useState("")
    const [updateDescription, setUpdateDescription] = useState("")
    const [updatePrice, setUpdatePrice] = useState("")
    const [updateCategory, setUpdateCategory] = useState("")
    const [updateProductImage, setUpdateProductImage] = useState("")



    const navigate = useNavigate();

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

  
    
 
    //MODAL SECTION
    const showModal = () => {
        setOpen(true);
      };
      const handleOk = () => {
        setLoading(true);
        setTimeout(() => {
          setLoading(false);
          setOpen(false);
        }, 3000);
      };
      const handleCancel = () => {
        setOpen(false);
      };
      /// update Section
      function updateProduct(event){
        event.preventDefault();
    
        fetch(`${process.env.REACT_APP_API_URL}/product/update/`+ adminProductProp._id, {
            method: "PUT",
            body: JSON.stringify({
             
              productName: updateProductname,
              description: updateDescription,
              price: updatePrice,
              category: updateCategory,
              productImage: updateProductImage
        

            }),
            headers:{
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(result => result.json())
          .then(data => {
            if(data){
              window.location.reload(false);
              Swal.fire({
            title: "Successfully Registered",
            icon: "success",
            text: "Please login you account"
          })
               
            
            } else {
                Modal.error({
                    title: 'Update failed',
                    content: 'Please Try Again',
                  });
            }
          })
        }
     
            const archiveProduct = (event) => {
                event.preventDefault();
           
               fetch(`${process.env.REACT_APP_API_URL}/product/archiveProduct/${_id}`, {
                   method: "PUT",

                   headers:{
                    
                       Authorization: `Bearer ${localStorage.getItem('token')}`
                   }
                 })
                 .then(result => result.json())
                 .then(data => {
                   console.log(data)
                   if(data){
                      window.location.reload(false);
                     navigate("/AdminItems")
                   } else {
                       Modal.error({
                           title: 'Update failed',
                           content: 'Please Try Again',
                         });
                   }
                 })
               }
        
return(
        <>
            <Fragment>
            <tbody className="bg-white flex-1 sm:flex-none">
                <tr className="hover:bg-gray-100 flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                     <td className="p-4 text-clip overflow-hidden whitespace-normal text-sm font-normal text-gray-500">
                            <div className="text-base font-semibold text-gray-900">{productName}</div>
                               
                    </td>
                    <td className="p-4 text-clip overflow-hidden whitespace-normal text-base font-medium text-gray-900">{description}</td>
                    
                    <td className="p-4 text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{price}</td>
                    <td className="p-4 text-clip overflow-hidden whitespace-nowrap text-base font-medium text-gray-900">{category}</td>
                    <td >
                    <div className=" overflow-auto sm:flex items-center bottom-0 right-0  border-gray-200 hover:bg-gray-100">
                        <div className="flex items-center md:mt-4 mb-4 sm:mb-0">
                            <button type="button" onClick={showModal} className=" text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium inline-flex items-center justify-center rounded-lg text-sm px-3 py-2 text-center mr-4">Update</button>

                        {
                            isActive?
                            <button type="button"  onClick={(event)=> archiveProduct(event)} className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm inline-flex items-center px-3 py-2 text-center ">Enable</button>
                            :
                            <button type="button"  onClick={(event)=> archiveProduct(event)}  className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm inline-flex items-center px-3 py-2 text-center ">Disable</button>
                            

                        }
                            
                            
                        </div>
                       
                    </div>
                    </td>  
                </tr> 
            </tbody>

        <Modal
        open={open}
        title={_id}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Return
          </Button>,
        ]}
        >        
                 <h3 class="text-xl font-semibold">
                Edit product
                </h3>
                <div class="p-6 space-y-6">
                <form onSubmit={event => updateProduct(event)}>
               
                <div class="col-span-6 sm:col-span-3">
                <label for="product-name" class="text-sm font-medium text-gray-900 block mb-2">Product Name</label>
                <input 
                 value = {updateProductname}
                onChange ={event => setUpdateProductName(event.target.value)} 
                type="text" name="product-name" id="product-name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Product Name" ></input>
                
                <div class="grid grid-cols-6 gap-6">
                <div class="col-span-6 sm:col-span-3">
                <label for="category" class="text-sm font-medium text-gray-900 block mb-2">Category</label>
                <input 
                 value = {updateCategory}
                onChange ={event => setUpdateCategory(event.target.value)} 
                type="text" name="category" id="category" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="food / accesories" ></input>
                </div>
                <div class="col-span-6 sm:col-span-3">
                <label for="brand" class="text-sm font-medium text-gray-900 block mb-2">Image</label>
                <input 
                 value = {updateProductImage}
                onChange ={event => setUpdateProductImage(event.target.value)} 
                type="text" name="brand" id="brand" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="https" ></input>
                </div>
                <div class="col-span-6 sm:col-span-3">
                <label for="price" class="text-sm font-medium text-gray-900 block mb-2">Price</label>
                <input 
                 value = {updatePrice}
                onChange ={event => setUpdatePrice(event.target.value)} 
                type="number" name="price" id="price" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="php 100 " ></input>
                </div>
                <div class="col-span-full">
                <label for="product-details" class="text-sm font-medium text-gray-900 block mb-2">Product Details</label>
                <textarea 
                value = {updateDescription}
                onChange ={event => setUpdateDescription(event.target.value)} 
                id="product-details" rows="6" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4" placeholder="Description here"></textarea>
                </div>
                <Space wrap>
                <button 
               
                className="shadow bg-yellow-700 hover:bg-yellow-500 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                Save
                 </button>
                 </Space>
                </div>
                </div>
                </form>
                </div>
      </Modal>

      


            


            </Fragment>
            
    
       
        </>

    )
    
}
       
    